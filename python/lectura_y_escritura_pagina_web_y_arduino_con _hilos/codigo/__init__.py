'''
Created on 14-06-2013

@author: Luis Ortega
'''
from threading import Thread, Lock
import time
import sys
import serial
import urllib2 

# Hacemos una clase que herede de Thread.
# Si definimos un constructor __init__(), debemos
# obligatoriamente llamar al metodo __init__() de
# la clase Thread. Si no hacemos constructor, no
# es necesario.
class HiloLecturaYEscrituraEnArduino(Thread):
    # Se le pasa como ultimo parametro el Lock
    def __init__ (self, bloqueoEstados, bloqueoRegistros):
        Thread.__init__(self)
        self.bloqueoEstados = bloqueoEstados
        self.bloqueoRegistros = bloqueoRegistros

    
    # Metodo que se ejecutara en un hilo    
    def run (self):
        global estado
        global registro
        global serialArduino
        global paginaLectura
        global paginaEscritura
        while 1:
            registroTmp =serialArduino.readline();
            print registroTmp
            # Espera por Lock
            bloqueoRegistros.acquire()
            if registroTmp != registro:
                registro = registroTmp
            # Comienza la cuenta
            bloqueoRegistros.release()
            
            bloqueoEstados.acquire()
            estadoTmp=estado
            bloqueoEstados.release()
            serialArduino.write(estadoTmp);
            
class HiloLecturaDePaginaWeb(Thread):
    # Se le pasa como ultimo parametro el Lock
    def __init__ (self, bloqueoEstados, bloqueoRegistros):
        Thread.__init__(self)
        self.bloqueoEstados = bloqueoEstados
        self.bloqueoRegistros = bloqueoRegistros

    
    # Metodo que se ejecutara en un hilo    
    def run (self):
        global estado
        global registro
        global serialArduino
        global paginaLectura
        global paginaEscritura
        while 1:
            pagina = urllib2.urlopen(paginaLectura);
            lecturaEstados = pagina.read();
            pagina.close();
            # Espera por Lock
            bloqueoEstados.acquire()
            if lecturaEstados != estado:
                estado = lecturaEstados
            # Comienza la cuenta
            bloqueoEstados.release()

class HiloEscrituraDePaginaWeb(Thread):
    # Se le pasa como ultimo parametro el Lock
    def __init__ (self, bloqueoEstados, bloqueoRegistros):
        Thread.__init__(self)
        self.bloqueoEstados = bloqueoEstados
        self.bloqueoRegistros = bloqueoRegistros

    
    # Metodo que se ejecutara en un hilo    
    def run (self):
        global estado
        global registro
        global serialArduino
        global paginaLectura
        global paginaEscritura
        while 1:

            bloqueoRegistros.acquire()
            registroTmp = registro
            bloqueoRegistros.release()
            
            registroTmp = registroTmp.strip()
            lista=registroTmp.strip(":")#tokenizo el string
            mensaje ="?sa="+lista[0]+"sb="+lista[1]+"&sc="+lista[2]
            pagina = urllib2.urlopen(paginaEscritura+mensaje);
            pagina.read();
            pagina.close();


            
# Crea el hilo y lo arranca. Luego se pone a contar
if __name__ == '__main__':
    # Creacion del Lock y bloqueo del mismo
    bloqueoEstados = Lock()
    bloqueoEstados.acquire()
    bloqueoRegistros = Lock()
    bloqueoRegistros.acquire()
    #variables globales
    global estado
    global registro
    global serialArduino
    global paginaLectura
    global paginaEscritura
    
    estado="0:0:0"
    registro="0:0:0"
    
    puerto=12
    velocidad=9600
    try:
        serialArduino = serial.Serial(puerto, velocidad)
        serialArduino.timeout=1000;
    except:
        print "error en el arduino"
    
    paginaLectura ="fdb"
    # Arranque del hilo
    Arduino = HiloLecturaYEscrituraEnArduino(bloqueoEstados, bloqueoRegistros)
    Arduino.start()
    
    # Hacemos esperar al hilo
    time.sleep(1)
    # Un bucle
    for i in range (10,20):
        print "main = "+str(i)
        time.sleep(0.1)
       
    # Liberamos el bloqueo para que el hilo comience
    bloqueoEstados.release()
    bloqueoRegistros.release()
    
    while 1:
        print "lll "+registro