
//definición de pines usados
#include <stdlib.h>

int pin_sensor_a=A0;
int pin_sensor_b=A1;
int pin_sensor_c=A2;
int pin_actuador_a=11;
int pin_actuador_b=12;
int pin_actuador_c=13;

//variables
int cant_muestra=50;
String estado="0:0:0";/*aa:ab:ac*/
String registro="0:0:0";/*sa:sb:sc*/

void setup(){
  pinMode(pin_actuador_a, OUTPUT);   
  pinMode(pin_actuador_b, OUTPUT);   
  pinMode(pin_actuador_c, OUTPUT);           
  pinMode(pin_sensor_a, INPUT);     
  pinMode(pin_sensor_b, INPUT);  
  pinMode(pin_sensor_c, INPUT);   
  Serial.begin(9600);
}
void loop(){
  if(Serial.available()>0){
    String tmpEstado;
    tmpEstado=serialGetString();
    if(!estado.equals(tmpEstado)){
      estado=tmpEstado;
      digitalWrite(pin_actuador_a, estado.charAt(0)=='1');
      digitalWrite(pin_actuador_b, estado.charAt(2)=='1');
      digitalWrite(pin_actuador_c, estado.charAt(4)=='1');
    }   
  }
  else{
    String tmpRegistro=obtieneRegistro();
    if(!registro.equals(tmpRegistro)){
      registro=tmpRegistro;
      Serial.println(registro);
      delay(5000);
    }
  } 
}
String serialGetString(){
  String lectura;
  while (Serial.available()){
      lectura=lectura+(char)Serial.read();
      delayMicroseconds(500);
  }
  return lectura;
}
String obtieneRegistro(){
  String sa, sb, sc, registro;
  sa = (String)obtieneValor(pin_sensor_a);
  sb = (String)obtieneValor(pin_sensor_b);
  sc = (String)obtieneValor(pin_sensor_c);
  registro = sa+":"+sb+":"+sc;
  return registro;
}
int obtieneValor(int pin){
  int valor=0;
  for(int i=0;i<cant_muestra;i++){
    valor=valor+analogRead(pin);
  }
  return valor/cant_muestra;
}
